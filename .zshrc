#env variables
export EDITOR="nvim"

#default settings
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

#source antidote
source '/usr/share/zsh-antidote/antidote.zsh'
antidote load

#zoxide init
eval "$(zoxide init zsh)"

#custom aliases
alias update="yay -Syu"
alias inst="yay -S"
alias remove="yay -Rs"
alias search="yay -Ss"
alias pc="proxychains"
alias scr="cd ~/pictures && scrot -s -f -d 3 --format png"
alias nv="nvim"
alias snv="sudo nvim"
alias ff="fastfetch"
alias lg="lazygit"

#prompt
PROMPT=$'%{\e[0;34m%}%B┌─[%b%{\e[0m%}%{\e[1;32m%}%n%{\e[1;30m%}@%{\e[0m%}%{\e[0;36m%}%m%{\e[0;34m%}%B]%b%{\e[0m%} - %b%{\e[0;34m%}%B[%b%{\e[1;37m%}%~%{\e[0;34m%}%B]%b%{\e[0m%} - %{\e[0;34m%}%B[%b%{\e[0;33m%}'%D{"%a %b %d, %l:%M"}%b$'%{\e[0;34m%}%B]%b%{\e[0m%}
%{\e[0;34m%}%B└─%B[%{\e[1;35m%}$%{\e[0;34m%}%B]%{\e[0m%}%b '
PS2=$' \e[0;34m%}%B>%{\e[0m%}%b '

#run fastfetch at terminal start
fastfetch
