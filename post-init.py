#     _       _ __
#    (_)___  (_) /_
#   / / __ \/ / __/
#  / / / / / / /_
# /_/_/ /_/_/\__/
# created by insidesources to jumpstart your fresh install

import os
import subprocess


def run_command(command):
    process = subprocess.Popen(
        command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    stdout, stderr = process.communicate()
    if process.returncode != 0:
        raise Exception(f"command failed: {stderr.decode().strip()}")
    return stdout.decode().strip()


def run_command_interactive(command):
    subprocess.run(command, shell=True)


def install_ohmyzshplugins():
    print("cloning ohmyzsh plugins")
    run_command_interactive(
        "sh -c '$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)'"
    )
