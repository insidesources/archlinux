return {
	{
		"stevearc/oil.nvim",
		config = function()
			require("oil").setup(
				---@module 'oil'
				---@type oil.SetupOpts
			)
		end,
		opts = {
			show_hidden = true,
		},
		-- Optional dependencies
		-- dependencies = { { "echasnovski/mini.icons", opts = {} } },
		dependencies = { "nvim-tree/nvim-web-devicons" }, -- use if prefer nvim-web-devicons

		keys = {
			{ "<leader>t", "<cmd>Oil<cr>", desc = "open oil" },
		},
	},
}
