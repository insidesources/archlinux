return {
	"stevearc/conform.nvim",
	event = { "BufReadPre", "BufNewFile" },
	config = function()
		require("conform").setup({
			formatters_by_ft = {
				python = { "isort", "black" },
				lua = { "stylua" },
				html = { "prettier" },
				css = { "prettier" },
				markdown = { "prettier" },
			},
			format_after_save = {
				async = true,
				lsp_fallback = true,
			},
		})
	end,
	keys = {
		{
			"<leader>rf",
			function()
				require("conform").format({ async = true })
			end,
			desc = "format buffer",
		},
	},
}
