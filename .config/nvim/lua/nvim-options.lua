vim.cmd("colorscheme default")
vim.opt.termguicolors = false
vim.cmd("set relativenumber number")
vim.cmd("set clipboard=unnamedplus")
vim.opt_local.conceallevel = 2
vim.opt.guicursor = "a:ver20"
vim.opt.statuscolumn = "%s %l %r"
vim.opt.numberwidth = 8
