#          __  _ __
#   ____ _/ /_(_) /__
#  / __  / __/ / / _ \
# / /_/ / /_/ / /  __/
# \__  /\__/_/_/\___/
#   /_/
# config by insidesources

import os
import subprocess
from datetime import datetime
from os import system

# from qtile_extras.widget import StatusNotifier
import colors
from libqtile import bar, extension, hook, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy

# Make sure 'qtile-extras' is installed or this config will not work.
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration

if qtile.core.name == "x11":
    term = "alacritty"
elif qtile.core.name == "wayland":
    term = "alacritty"

mod = "mod4"
myTerm = "ghostty"
myBrowser = "zen"
myMenu = "rofi -show run"
myObsidian = "obsidian"
myScreenshots = "~/pictures/"
myFiles = "doublecmd"
myPowermenu = "rofi -show power-menu -modi 'power-menu:rofi-power-menu --choices=reboot/shutdown/suspend/logout'"
myDiscord = "discord"
myWallpaper = "~/pictures/ascii-art-custom.png"
myWallpapermode = "fill"
myLocker = "slock"
myNetworkManager = "networkmanager_dmenu"

# screenshot function


@lazy.function
def screenshot(_qtile, mode=0):
    file_path = datetime.now().strftime(f"{myScreenshots}%m-%d-%Y-%H%M.png")
    system(f"scrot {'-s -f' if mode == 1 else ''} {file_path}")
    system(f"xclip -selection clipboard -t image/png -i {file_path}")


# A list of available commands that can be bound to keys can be found
# at https://docs.qtile.org/en/latest/manual/config/lazy.html
keys = [
    Key([mod], "Return", lazy.spawn(myTerm), desc="terminal"),
    Key([mod], "b", lazy.spawn(myBrowser), desc="web browser"),
    Key([mod], "n", lazy.spawn(myObsidian), desc="obsidian"),
    Key([mod], "e", lazy.spawn(myFiles), desc="doublecmd"),
    Key([mod], "d", lazy.spawn(myDiscord), desc="discord"),
    Key([mod], "l", lazy.next_layout(), desc="Toggle to next layout"),
    Key([mod, "control"], "l", lazy.spawn(myLocker), desc="slock"),
    Key([mod, "shift"], "l", lazy.prev_layout(),
        desc="Toggle to previous layout"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "Delete", lazy.spawn(
        myPowermenu), desc="Launch powermenu"),
    Key([mod], "r", lazy.spawn(myMenu), desc="spawn rofi to run an application"),
    Key([], "Print", screenshot(), desc="Take a screenshot"),
    Key(["mod1"], "Print", screenshot(mode=1),
        desc="snipping tool with alt + print"),
    # Switch between windows
    # Some layouts like 'monadtall' only need to use j/k to move
    # through the stack, but other layouts like 'columns' will
    # require all four directions h/j/k/l to move around.
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "tab", lazy.layout.next(),
        desc="Move window focus to other window"),
    Key(
        [mod, "shift"],
        "Left",
        lazy.layout.shuffle_left(),
        desc="Move window to the left",
    ),
    Key(
        [mod, "shift"],
        "Right",
        lazy.layout.shuffle_right(),
        desc="Move window to the right",
    ),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),
    Key(
        [mod],
        "equal",
        lazy.layout.grow().when(layout=["monadtall", "monadwide"]),
        desc="Grow window to the left",
    ),
    Key(
        [mod],
        "minus",
        lazy.layout.shrink().when(layout=["monadtall", "monadwide"]),
        desc="Grow window to the right",
    ),
    # window control and resizing
    # Key([mod, "control"], "Left", lazy.layout.grow_left(), desc="Grow window to the left"),
    # Key([mod, "control"], "Right", lazy.layout.grow_right(), desc="Grow window to the right"),
    # Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    # Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    # Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], "f", lazy.window.toggle_floating(), desc="toggle floating"),
    Key([mod], "m", lazy.window.toggle_fullscreen(), desc="toggle fullscreen"),
    # Switch focus of monitors
    Key([mod], "period", lazy.next_screen(),
        desc="Move focus to next monitor"),
    Key([mod], "comma", lazy.prev_screen(), desc="Move focus to prev monitor"),
    # Sound
    Key(
        [mod],
        "o",
        lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle"),
        desc="mutes or unmutes the audio",
    ),
    Key(
        [mod],
        "i",
        lazy.spawn("pactl set-source-mute @DEFAULT_SOURCE@ toggle"),
        desc="mutes or unmutes the microphone",
    ),
    Key(
        [mod],
        "j",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -2%"),
        desc="lowers the audio",
    ),
    Key(
        [mod],
        "k",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +2%"),
        desc="raises the audio",
    ),
    Key(
        [mod],
        "p",
        lazy.spawn("playerctl --player=spotify,%any play-pause"),
        desc="play or pause audio",
    ),
    Key(
        [mod, "control"],
        "j",
        lazy.spawn("playerctl --player=spotify,%any previous"),
        desc="goes to the previous track",
    ),
    Key(
        [mod, "control"],
        "k",
        lazy.spawn("playerctl --player=spotify,%any next"),
        desc="goes to the next track",
    ),
    # Emacs programs launched using the key chord CTRL+e followed by 'key'
    # KeyChord([mod],"e", [
    #    Key([], "e", lazy.spawn(myEmacs), desc='Emacs Dashboard'),
    #    Key([], "a", lazy.spawn(myEmacs + "--eval '(emms-play-directory-tree \"~/Music/\")'"), desc='Emacs EMMS'),
    #    Key([], "b", lazy.spawn(myEmacs + "--eval '(ibuffer)'"), desc='Emacs Ibuffer'),
    #    Key([], "d", lazy.spawn(myEmacs + "--eval '(dired nil)'"), desc='Emacs Dired'),
    #    Key([], "i", lazy.spawn(myEmacs + "--eval '(erc)'"), desc='Emacs ERC'),
    #    Key([], "s", lazy.spawn(myEmacs + "--eval '(eshell)'"), desc='Emacs Eshell'),
    #    Key([], "v", lazy.spawn(myEmacs + "--eval '(vterm)'"), desc='Emacs Vterm'),
    #    Key([], "w", lazy.spawn(myEmacs + "--eval '(eww \"distro.tube\")'"), desc='Emacs EWW'),
    #    Key([], "F4", lazy.spawn("killall emacs"),
    #                  lazy.spawn("/usr/bin/emacs --daemon"),
    #                  desc='Kill/restart the Emacs daemon')
    # ]),
    # Dmenu scripts launched using the key chord SUPER+p followed by 'key'
    #    KeyChord([mod], "p", [
    #        Key([], "h", lazy.spawn("dm-hub"), desc='List all dmscripts'),
    #        Key([], "a", lazy.spawn("dm-sounds"), desc='Choose ambient sound'),
    #        Key([], "b", lazy.spawn("dm-setbg"), desc='Set background'),
    #        Key([], "c", lazy.spawn("dtos-colorscheme"), desc='Choose color scheme'),
    #        Key([], "e", lazy.spawn("dm-confedit"), desc='Choose a config file to edit'),
    #        Key([], "i", lazy.spawn("dm-maim"), desc='Take a screenshot'),
    #        Key([], "k", lazy.spawn("dm-kill"), desc='Kill processes '),
    #        Key([], "m", lazy.spawn("dm-man"), desc='View manpages'),
    #        Key([], "n", lazy.spawn("dm-note"), desc='Store and copy notes'),
    #        Key([], "o", lazy.spawn("dm-bookman"), desc='Browser bookmarks'),
    #        Key([], "p", lazy.spawn("passmenu -p \"Pass: \""), desc='Logout menu'),
    #        Key([], "q", lazy.spawn("dm-logout"), desc='Logout menu'),
    #        Key([], "r", lazy.spawn("dm-radio"), desc='Listen to online radio'),
    #        Key([], "s", lazy.spawn("dm-websearch"), desc='Search various engines'),
    #        Key([], "t", lazy.spawn("dm-translate"), desc='Translate text')
    #    ])
    # spawn rofi
    # Key([mod, "p"],lazy.spawn("rofi -show run", desc='run rofi launcher')),
]
groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

# group_labels = [" term", " web", " systems", " obsidian", " code", " hack", " admin", " games", " chat", " overflow"]
# group_labels = ["1 - term", "2 - web", "3 - systems", "4 - obsidian", "5 - code", "6 - games", "7 - misc", "8 - hacking", "9 - admin"]
# group_labels = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
group_labels = [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "zero",
]
# group_labels = [" ", "", " ", " ", " ", " ", " ", " ", " ", " "]

group_layouts = [
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
]


for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        )
    )

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),
            ),
        ]
    )


# colorscheme
# Colors are defined in a separate 'colors.py' file.

colors = colors.darkstar

# layouts
layout_theme = {
    "border_width": 3,
    "margin": 1,
    "border_focus": colors[16],
    "border_normal": colors[0],
}

layouts = [
    layout.Matrix(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Max(
        border_width=0,
        margin=0,
    ),
]

floating_layout = layout.Floating(
    border_focus=colors[16],
    border_width=3,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        # example: Match(wm_class="confirmreset"),   # gitk
    ],
)

# Some settings that I use on almost every widget, which saves us
# from having to type these out for each individual widget.
widget_defaults = dict(
    font="source code pro",
    fontsize=11,
    padding=3,
    background=colors[0],
)

extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
        # widget.Image(
        #    filename="~/.config/qtile/icons/archlogo.png",
        #    scale="False",
        #    mouse_callbacks={"Button1": lambda: qtile.cmd_spawn(myMenu)},
        # ),
        widget.TextBox(
            fmt="menu",
            mouse_callbacks={"Button1": lambda: qtile.cmd_spawn(myMenu)},
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],  # top right bottom left
                )
            ],
        ),
        widget.GroupBox(
            borderwidth=3,
            margin_y=5,
            active=colors[1],
            inactive=colors[1],
            rounded=False,
            highlight_color=colors[0],
            highlight_method="line",
            this_current_screen_border=colors[16],
            this_screen_border=colors[10],
            other_current_screen_border=colors[10],
            other_screen_border=colors[10],
            hide_unused=True,
        ),
        widget.Spacer(length=20),
        widget.WindowTabs(
            separator=" ",
            foreground=colors[1],
            scroll_fixed_width=False,
            scroll=True,
            # max_chars = 100,
            width=700,
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],  # top right bottom left
                )
            ],
        ),
        widget.Spacer(length=bar.STRETCH),
        widget.Net(
            format="↓{down:.0f}{down_suffix} ↑{up:.0f}{up_suffix}",
            fmt="eth:{}",
            # width = 120,
            interface="wlan0",
            use_bits=True,
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(
                    myBrowser + " https://www.whatsmyip.org"
                )
            },
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.Wlan(
            foreground=colors[1],
            format="{essid}",
            fmt="{}",
            # visible_on_warn = False,
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(myNetworkManager),
                "Button3": lambda: qtile.cmd_spawn(myTerm + " -e nmcli dev wifi list")
            },
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.Battery(
            format="{hour:d}:{min:02d} {char}{percent:1.0%}",
            foreground=colors[1],
            charge_char="↑",
            discharge_char="↓",
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.Volume(
            foreground=colors[1],
            fmt="vol:{}",
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
            volume_app="pavucontrol",
        ),
        widget.Spacer(length=3),
        widget.CheckUpdates(
            distro="Arch_yay",
            display_format="updates:{updates}",
            update_interval=43200,
            no_update_string="updated",
            initial_text="syncing",
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(myTerm + " -e yay -Syu")
            },
            foreground=colors[1],
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],  # top right bottom left
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.GenPollText(
            update_interval=300,
            func=lambda: subprocess.check_output(
                "printf $(uname -r)", shell=True, text=True
            ),
            foreground=colors[1],
            fmt="{}",
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.CPU(
            format="cpu:{load_percent}%",
            foreground=colors[1],
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(myTerm + " -e btop")},
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.Memory(
            # widget12
            foreground=colors[1],
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(myTerm + " -e btop")},
            format="{MemUsed:.0f}{mm}",
            fmt="ram:{}",
            measure_mem="M",
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.DF(
            update_interval=60,
            foreground=colors[1],
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(myTerm + " -e mc ~/")},
            partition="/",
            # format = '[{p}] {uf}{m} ({r:.0f}%)',
            format="{uf}{m}",
            fmt="/:{}",
            visible_on_warn=False,
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.Clock(
            foreground=colors[1],
            format="%a, %b %d %l:%M%p",
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(myTerm + " -e calcurse")
            },
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=3),
        widget.TextBox(
            # widget22
            text="power menu",
            foreground=colors[1],
            mouse_callbacks={"Button1": lambda: qtile.cmd_spawn(myPowermenu)},
            decorations=[
                BorderDecoration(
                    colour=colors[9],
                    border_width=[0, 0, 3, 0],
                )
            ],
        ),
        widget.Spacer(length=2),
        # widget.Systray(padding = 3),
        # widget.Spacer(length = 2),
    ]
    return widgets_list


# Monitor 1 will display ALL widgets in widgets_list. It is important that this
# is the only monitor that displays all widgets because the systray widget will
# crash if you try to run multiple instances of it.
def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1


# All other monitors' bars will display everything but widgets 22 (systray) and 23 (spacer).
def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    # del widgets_screen2[22]
    # del widgets_screen2[23]
    return widgets_screen2


# For adding transparency to your bar, add (background="#00000000") to the "Screen" line(s)
# For ex: Screen(top=bar.Bar(widgets=init_widgets_screen2(), background="#00000000", size=24)),


def init_screens():
    return [
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen1(), background="#00000000", size=25
            ),
            wallpaper=myWallpaper,
            wallpaper_mode=myWallpapermode,
        ),
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen1(), background="#00000000", size=25
            ),
            wallpaper=myWallpaper,
            wallpaper_mode=myWallpapermode,
        ),
    ]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

follow_mouse_focus = False
bring_front_click = True
cursor_warp = False

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False

# When using the Wayland backend, this can be used to configure input devices.
# wl_input_rules = None


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/autostart.sh"])


wmname = "qtile"