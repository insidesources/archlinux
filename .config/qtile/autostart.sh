#!/bin/sh

#   ___ _____ ___ _     _____   ____  _             _    
#  / _ \_   _|_ _| |   | ____| / ___|| |_ __ _ _ __| |_  
# | | | || |  | || |   |  _|   \___ \| __/ _` | '__| __| 
# | |_| || |  | || |___| |___   ___) | || (_| | |  | |_  
#  \__\_\|_| |___|_____|_____| |____/ \__\__,_|_|   \__| 
#   
# config by insidesources

#autostart applications
lxappearance --nowindow &
picom &
#streamdeck &

# 3. Uncomment to set wallpaper with nitrogen
#nitrogen --restore &
