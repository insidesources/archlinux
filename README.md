Hello,

This is my arch linux daily driver configuration!
```
OS: arch linux

WM: qtile (X11)

Kernel: linux-zen

Terminal: ghostty

Shell: zsh

Theme: darkstar

Icons: arc

Font: source code pro

Cursor: Bibata-Original-Ice
```
In this you can find my system-init python script to run after you do a fresh install, to pull core packages and do some system configuration!

Some other configs you might find useful:
1. ghostty
2. alacritty
3. dunst
4. nvim
5. qtile
6. rofi
7. spicetify
8. tmux
9. zsh

Other goodies:
1. obsidian custom skin
2. midnight commander custom skin

Good luck and have fun! -insidesources