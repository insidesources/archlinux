#     _       _ __
#    (_)___  (_) /_
#   / / __ \/ / __/
#  / / / / / / /_
# /_/_/ /_/_/\__/
# created by insidesources to jumpstart your fresh install

import os
import subprocess


def run_command(command):
    process = subprocess.Popen(
        command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    stdout, stderr = process.communicate()
    if process.returncode != 0:
        raise Exception(f"command failed: {stderr.decode().strip()}")
    return stdout.decode().strip()


def run_command_interactive(command):
    subprocess.run(command, shell=True)


def install_yay():
    print("installing yay")
    run_command_interactive("git clone https://aur.archlinux.org/yay.git")
    os.chdir("yay")
    run_command_interactive("makepkg -si")
    os.chdir("..")
    run_command("rm -rf yay")
    print("yay installed successfully")


def install_blackarch():
    print("installing BlackArch")
    run_command("curl -O https://blackarch.org/strap.sh")
    run_command("chmod +x strap.sh")
    run_command_interactive("sudo ./strap.sh")
    run_command("rm strap.sh")
    run_command_interactive("yay -Syu")
    print("BlackArch installed successfully.")


def install_packages(category, packages):
    response = input(f"do you want to install {
                     category} packages? (yes/no): ").lower()
    if response.startswith("y"):
        for package in packages:
            print(f"installing {package}...")
            run_command_interactive(f"yay -S {package}")
    elif response.startswith("n"):
        print(f"skipping installation of {category} packages")


def clone_and_copy_repo():
    print("cloning your git repository...")
    run_command("git clone https://gitlab.com/insidesources/archlinux.git")
    os.chdir("archlinux")
    print("copying files to the home directory")
    run_command_interactive("sudo cp -ri . ~/")


def change_default_shell():
    print("changing the default shell to ZSH")
    run_command_interactive("chsh -s /usr/bin/zsh")


def main():
    try:
        install_yay()
        install_blackarch()

        # package lists
        system_packages = [
            "alsa-utils",
            "linux-zen-headers",
        ]

        custom_packages = [
            "alacritty",
            "arc-gtk-theme",
            "arc-icon-theme",
            "bibata-cursor-theme-bin",
            "betterdiscord-installer-bin",
            "bitwarden",
            "brave-bin",
            "brightnessctl",
            "btop",
            "calcurse",
            "cliphist",
            "cuda",
            "cuda-tools",
            "debtap",
            "discord",
            "docker",
            "doublecmd",
            "dunst",
            "easyeffects",
            "entr",
            "fastfetch",
            "feh",
            "filezilla",
            "firefox",
            "flat-remix-gtk",
            "fzf",
            "glances",
            "gimp",
            "gotop",
            "hypridle",
            "hyprlock",
            "hyprpaper",
            "hyprshot",
            "lazygit",
            "ledger-live-bin",
            "lxqt-config",
            "lynx",
            "man",
            "mc",
            "mysql-workbench",
            "neovim",
            "noto-fonts-emoji",
            "npm",
            "nvidia-container-toolkit",
            "nvidia-settings",
            "nvtop",
            "nwg-look",
            "obs-studio-browser",
            "obsidian",
            "ollama-cuda",
            "onlyoffice-bin",
            "openrgb",
            "openvpn",
            "pavucontrol",
            "playerctl",
            "postman-bin",
            "powershell-bin",
            "proton-vpn-gtk-app",
            "proxychains-ng",
            "qt5ct",
            "qt5gtk2",
            "qt5-styleplugins",
            "qt6ct",
            "qt6gtk2",
            "qalculate-gtk",
            "qflipper",
            "qbittorrent",
            "qutebrowser",
            "rpi-imager",
            "solaar",
            "spicetify-cli",
            "spotify",
            "streamdeck-ui",
            "teamviewer",
            "telegram-desktop",
            "tmux",
            "tmux-plugin-manager",
            "tor",
            "torbrowser-launcher",
            "tradingview",
            "ttf-font-awesome",
            "unicode-character-database",
            "unicode-emoji",
            "vlc",
            "waybar",
            "weechat",
            "wl-clipboard",
            "wl-clip-persist",
            "xclip",
            "xreader",
            "zen-browser-bin",
            "zerotier-one",
            "zoxide",
            "zsh",
        ]

        cybersecurity_tools = [
            "dnsrecon",
            "exploitdb",
            "hydra",
            "metasploit",
            "ngrok",
            "nmap",
            "phonesploit",
            "punter",
            "recon-ng",
            "set",
            "sublist3r",
            "thefatrat",
            "theharvester",
            "wireshark-qt",
            "wpscan",
        ]

        games = ["steam", "lutris", "proton-ge-custom-bin", "protontricks"]

        install_packages("system packages", system_packages)
        install_packages("custom packages", custom_packages)
        install_packages("cybersecurity tools", cybersecurity_tools)
        install_packages("games", games)

        clone_and_copy_repo()

        change_default_shell()

    except Exception as e:
        print(f"An error occurred: {e}")


if __name__ == "__main__":
    main()
